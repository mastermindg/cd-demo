FROM jekyll/jekyll:3.2.1

ARG SETTING=default
ENV MYARG=$SETTING

ADD site /srv/jekyll
CMD jekyll serve
